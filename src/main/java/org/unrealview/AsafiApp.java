/*
 * AsafiApp.java                                   date: 02/05/2020 15:34
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview;

import main.java.org.unrealview.model.GlobalSettings;
import main.java.org.unrealview.model.automation.Pool;
import main.java.org.unrealview.model.automation.Profile;
import main.java.org.unrealview.model.file.Reader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static java.lang.System.exit;
import static main.java.org.unrealview.model.file.FilePath.*;

/**
 * Main launcher.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class AsafiApp {

    private static final Logger LOGGER = LogManager.getLogger("main");

    /**
     * Start Asafi app: load GlobalSettings and main profile.
     *
     * @param args optionally used for the root configuration folder path.
     */
    public static void main(String[] args) {

        LOGGER.info("Starting ASAFI! v0.0.71");


        ////////////////////////////////////////////////////////////////////////
        // Check file tree.
        ////////////////////////////////////////////////////////////////////////


        if (args.length > 0 && Files.isDirectory(Path.of(args[0]))) {
            root = args[0];
        }

        Path path = Path.of(root + sep + ROOT);
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("Config directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create path directory!");
            }
            LOGGER.fatal("Config directory does not exist!");
        } else {
            LOGGER.info("Config directory OK.");
        }

        path = Path.of(root + sep + ROOT + sep + SETTINGS);
        if (Files.notExists(path)) {
            LOGGER.fatal("Settings file does not exist!");
            exit(1);
        } else {
            LOGGER.info("Settings file OK.");
        }

        path = Path.of(root + sep + ROOT + sep + PROFILES);
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("Profiles directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create profiles directory!");
            }
        } else {
            LOGGER.info("Profiles directory OK.");
        }

        path = Path.of(root + sep + ROOT + sep + DATA);
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("Data directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create data directory!");
            }
        } else {
            LOGGER.info("Data directory OK.");
        }

        path = Path.of(root + sep + ROOT + sep + DATA + sep + BY_LIST);
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("by_list directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create by_list directory!");
            }
        } else {
            LOGGER.info("by_list directory OK.");
        }

        path = Path.of(root + sep + ROOT + sep + DATA + sep + ONE_BY_ONE);
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("one_by_one directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create one_by_one directory!");
            }
        } else {
            LOGGER.info("one_by_one directory OK.");
        }

        path = Path.of(root +
                       sep +
                       ROOT +
                       sep +
                       DATA +
                       sep +
                       ONE_BY_ONE +
                       sep +
                       "users");
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("one_by_one users directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create one_by_one users directory!");
            }
        } else {
            LOGGER.info("one_by_one users directory OK.");
        }

        path = Path.of(root +
                       sep +
                       ROOT +
                       sep +
                       DATA +
                       sep +
                       ONE_BY_ONE +
                       sep +
                       "posts");
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("one_by_one posts directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create one_by_one posts directory!");
            }
        } else {
            LOGGER.info("one_by_one posts directory OK.");
        }

        path = Path.of(root + sep + ROOT + sep + DATA + sep + STATS);
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(path);
                LOGGER.info("statistics directory has been created.");
            } catch (IOException e) {
                LOGGER.fatal("Can't create statistics directory!");
            }
        } else {
            LOGGER.info("statistics directory OK.");
        }


        ////////////////////////////////////////////////////////////////////////
        // Load settings file.
        ////////////////////////////////////////////////////////////////////////


        GlobalSettings globalSettings;
        try {
            globalSettings = Reader.readSettings();
        } catch (Exception e) {
            LOGGER.fatal("Loading of global parameters failed.\nCause: {}",
                         e.getMessage());
            exit(1);
            return;
        }

        LOGGER.info("Global settings have been loaded.");

        Map<String, Profile> profiles;
        try {
            profiles = Reader.readProfiles();
            LOGGER.info("Profiles have been read.");
        } catch (Exception e) {
            LOGGER.fatal("Can't read profiles!\nCause: {}", e.getMessage());
            e.printStackTrace();
            exit(1);
            return;
        }

        globalSettings.profilePlans.forEach(pflPl -> {
            if (!profiles.containsKey(pflPl.name + YML_EXT)) {
                LOGGER.fatal("A configuration profile plan ({}) does not have" +
                             " a corresponding profile!", pflPl.name);
                exit(1);
            }
            pflPl.profile = profiles.get(pflPl.name + YML_EXT);
        });

        LOGGER.info("Profiles have been linked to their corresponding profile" +
                    " plan.");


        ////////////////////////////////////////////////////////////////////////
        // Effectively start profiles in pool.
        ////////////////////////////////////////////////////////////////////////


        try {
            new Pool(globalSettings, Reader.readData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
