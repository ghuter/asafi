/*
 * ProfilePlan.java                              date: 09/08/2021 10:02
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.schedule;

import main.java.org.unrealview.model.automation.Profile;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class ProfilePlan {

    /** Name of file profile without extension. */
    public String name;

    /** First delay before execution in seconds. */
    public long delay;

    /** Delay between execution in seconds. */
    public long period;

    /**
     * Number of repetition : stop at 0 value. Set negative value to make
     * infinite loop.
     */
    public int repeat;

    public Profile profile;

    public ProfilePlan() {
        delay = 0;
        period = 1;
        repeat = 1;
    }
}
