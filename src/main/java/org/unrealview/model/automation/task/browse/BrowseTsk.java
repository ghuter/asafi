/*
 * BrowseTsk.java                                   date: 26/06/2020 23:26
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.browse;

import main.java.org.unrealview.model.automation.object.InstaObj;
import main.java.org.unrealview.model.automation.task.AsafiTsk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static main.java.org.unrealview.model.automation.Property.DEFAULT_NUMERIC_VALUE;
import static main.java.org.unrealview.model.automation.Property.FIFO;
import static main.java.org.unrealview.model.automation.Property.LIFO;
import static main.java.org.unrealview.model.automation.Property.RANDOM;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public abstract class BrowseTsk extends AsafiTsk {

    /** Value for the type if the previous node is not used (optional). */
    public String value;

    /** The type of list path: FIFO, LIFO, RANDOM. */
    public String path;

    /** Quantity of result may be found. */
    public int amount;


    /** TODO describe this attribute */
    protected List<InstaObj> results;

    /** TODO describe this attribute */
    public List<AsafiTsk> tasks;

    public BrowseTsk() {

        amount = DEFAULT_NUMERIC_VALUE;
        path = FIFO;

        results = new ArrayList<>();
        tasks = new ArrayList<>();
    }

    @Override
    public void execute(InstaObj current) {

        fillList();

        switch (path) {

        case FIFO -> { }
        case LIFO -> Collections.reverse(results);
        case RANDOM -> Collections.shuffle(results);
        default -> throw new IllegalStateException("Unexpected value for " +
                                                   "path: " +
                                                   path);
        }

        results.forEach(obj -> tasks.forEach(tsk -> {
            tsk.interpreter = interpreter;
            tsk.execute(obj);
        }));
    }

    /**
     * TODO describe this method
     */
    protected abstract void fillList();
}
