/*
 * SaveObOUser.java                              date: 09/20/2021 17:43
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.insta;

import main.java.org.unrealview.model.automation.object.User;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class SaveObOUser extends HumanTsk {

    public String name;

    public SaveObOUser() { name = ""; }

    @Override
    public boolean make() {

        if (name.isEmpty()) { return false; }
        if (current == null || !(current instanceof User)) { return false; }

        User usr = (User) current;

        interpreter.pool.fileDataContext.addEntryForObO(usr, name);

        return true;
    }
}
