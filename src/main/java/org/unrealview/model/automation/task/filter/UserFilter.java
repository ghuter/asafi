/*
 * UserFilter.java                               date: 06/05/2020 18:25
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.filter;

import main.java.org.unrealview.model.automation.object.User;

import static main.java.org.unrealview.model.automation.Property.DEFAULT_NUMERIC_VALUE;

/**
 * User filter settings.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class UserFilter extends InstaObjFilter {

    /** TODO describe this attribute */
    private final double minRatio;

    /** TODO describe this attribute */
    private final double maxRatio;

    public UserFilter() {
        minRatio = DEFAULT_NUMERIC_VALUE;
        maxRatio = DEFAULT_NUMERIC_VALUE;
    }

    /**
     * Tests each filter point that are set up.
     *
     * @return true if the post passes the filter
     */
    public boolean isMatch() {

        if (current == null || !(current instanceof User)) { return false; }

        User   usr = (User) current;
        String sc  = usr.getStringValues().get("username");

        if (!super.preMatch(usr, sc)) { return false; }

        float ratio = (float) usr.getIntValues().get("followers amount") /
                      usr.getIntValues().get("following amount");

        if (minRatio != DEFAULT_NUMERIC_VALUE && ratio < minRatio) {
            LOGGER.info("The user ({}) does not match the filter. For " +
                        "'minRatio' filter, value is: {}, " +
                        "ratio: {}", sc, minRatio, ratio);
            return false;
        }

        if (maxRatio != DEFAULT_NUMERIC_VALUE && ratio > maxRatio) {
            LOGGER.info("The user ({}) does not match the filter. For " +
                        "'maxRatio' filter, value is: {}, " +
                        "ratio: {}", sc, maxRatio, ratio);
            return false;
        }

        String description = usr.getStringValues().get("description");

        String regexInclude = stringValues.get("description include pattern");
        if (regexInclude != null && !description.matches(regexInclude)) {
            LOGGER.info("The user ({}) does not match the filter. For " +
                        "'description include pattern' filter, regex is: {}, " +
                        "description: {}", sc, regexInclude, description);

            return false;
        }

        String regexExclude = stringValues.get("description exclude pattern");
        if (regexExclude != null && description.matches(regexExclude)) {
            LOGGER.info("The user ({}) does not match the filter. For " +
                        "'description exclude pattern' filter, regex is: {}, " +
                        "description: {}", sc, regexExclude, description);
            return false;
        }

        return true;
    }
}
