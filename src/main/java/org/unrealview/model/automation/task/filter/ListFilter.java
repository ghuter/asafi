/*
 * ListFilter.java                               date: 09/19/2021 15:30
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.filter;

import main.java.org.unrealview.model.automation.object.InstaObj;
import main.java.org.unrealview.model.automation.task.AsafiTsk;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public abstract class ListFilter extends FilterTsk {

    private boolean isPreMod;

    private boolean isFirst;

    protected String type;

    /** TODO describe this attribute */
    protected List<InstaObj> objs;

    /** TODO describe this attribute */
    public List<AsafiTsk> inputTasks;

    public ListFilter() {
        isPreMod = true;
        isFirst = true;
        objs = new ArrayList<>();
    }

    @Override
    public void execute(InstaObj current) {

        if (isFirst) {
            isFirst = false;
            inputTasks.forEach(tsk -> {
                tsk.interpreter = interpreter;
                tsk.execute(null);
            });
            isPreMod = false;
        }

        if (isPreMod) {
            objs.add(current);
            return;
        }

        super.execute(current);
    }
}
