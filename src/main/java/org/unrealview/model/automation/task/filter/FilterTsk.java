/*
 * FilterTsk.java                                   date: 09/16/2021 09:55
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.filter;

import main.java.org.unrealview.model.automation.object.InstaObj;
import main.java.org.unrealview.model.automation.task.AsafiTsk;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public abstract class FilterTsk extends AsafiTsk {

    /** TODO describe this attribute */
    public List<AsafiTsk> tasksYes;

    /** TODO describe this attribute */
    public List<AsafiTsk> tasksNo;

    public FilterTsk() {
        tasksYes = new ArrayList<>();
        tasksNo = new ArrayList<>();
    }

    @Override
    public void execute(InstaObj current) {

        super.execute(current);

        if (isMatch()) {
            tasksYes.forEach(tsk -> {
                tsk.interpreter = interpreter;
                tsk.execute(current);
            });
        } else {
            tasksNo.forEach(tsk -> {
                tsk.interpreter = interpreter;
                tsk.execute(current);
            });
        }
    }

    /**
     * TODO describe this method
     */
    public abstract boolean isMatch();
}
