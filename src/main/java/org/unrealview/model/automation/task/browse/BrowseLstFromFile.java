/*
 * BrowseLstFromFile.java                 date: 09/20/2021
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.browse;

import main.java.org.unrealview.model.automation.object.User;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static main.java.org.unrealview.model.file.FileDataContext.SEP_NAME;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class BrowseLstFromFile extends BrowseTsk {

    public String username;

    public String category;

    public long day;

    public BrowseLstFromFile() {
        username = "";
        category = "";
        day = 0;
    }

    @Override
    protected void fillList() {

        if (username.isEmpty() || category.isEmpty()) {
            LOGGER.warn("username and category is needed for BrowseLstFromFile. ");
            return;
        }

        HashMap<String, ArrayList<User>> map =
                interpreter.pool.fileDataContext.getUsersByLists()
                                                .get(Instant.now()
                                                            .minus(day,
                                                                   ChronoUnit.DAYS)
                                                            .truncatedTo(
                                                                    ChronoUnit.DAYS));
        if (map == null) {
            LOGGER.warn("BrowseLstFromFile can't find values with " +
                        "this day value: category={}, username={}, day={}.",
                        category,
                        username,
                        day);
            return;
        }

        List<User> lst = map.get(category + SEP_NAME + username);

        if (lst == null) {
            LOGGER.warn("BrowseLstFromFile can't find values with " +
                        "this username: category={}, username={}, day={}.",
                        category,
                        username,
                        day);
        } else {
            LOGGER.info("BrowseLstFromFile find {} values.", lst.size());
            results.addAll(lst);
        }
    }
}
