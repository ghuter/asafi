/*
 * CommentFilter.java                            date: 06/05/2020 18:27
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.filter;

import java.time.LocalDate;

/**
 * Comment filter settings.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class CommentFilter {

    /** Date after which the comment must be posted. */
    private final LocalDate before;

    /** Date by which the comment must be posted. */
    private final LocalDate after;

    /** The post on which the comment is made must be a video. */
    private final boolean isVideo;

    /** The post on which the comment is made must be a carousel. */
    private final boolean isCarousel;

    /** Amount of like minimum on the relative post. */
    private final int minLike;

    /** Maximum like amount on the relative post. */
    private final int maxLike;

    /** Minimum amount of comments on the relative post. */
    private final int minComment;

    /** Maximum amount of comments on the relative post. */
    private final int maxComment;

    public CommentFilter(LocalDate before, LocalDate after, boolean isVideo,
                         boolean isCarousel, int minLike, int maxLike,
                         int minComment, int maxComment) {

        this.before = before;
        this.after = after;
        this.isVideo = isVideo;
        this.isCarousel = isCarousel;
        this.minLike = minLike;
        this.maxLike = maxLike;
        this.minComment = minComment;
        this.maxComment = maxComment;
    }

    /**
     * Tests each filter point that are set up.
     *
     * @param comment the comment to be filtered
     * @return true if the post passes the filter
     */
    public boolean isMatch(
            main.java.org.unrealview.model.automation.object.Comment comment) {

        boolean isMatch = true;



        return isMatch;
    }
}