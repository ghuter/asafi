/*
 * InstaObjFilter.java                           date: 09/19/2021 15:50
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.filter;

import main.java.org.unrealview.model.automation.object.InstaObj;

import java.util.HashMap;
import java.util.Map;

import static main.java.org.unrealview.model.automation.Property.MAX_PREFIX;
import static main.java.org.unrealview.model.automation.Property.MIN_PREFIX;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public abstract class InstaObjFilter extends FilterTsk {


    /** TODO describe this attribute */
    protected final Map<String, Integer> intValues;

    /** TODO describe this attribute */
    protected final Map<String, Boolean> boolValues;

    /** TODO describe this attribute */
    protected final Map<String, String> stringValues;

    public InstaObjFilter() {
        boolValues = new HashMap<>();
        intValues = new HashMap<>();
        stringValues = new HashMap<>();
    }

    /**
     * Tests common attribut of InstaObj.
     *
     * @param obj the instagram object to be filtered
     * @return true if the post passes the filter
     */
    public boolean preMatch(InstaObj obj, String identifier) {

        for (Map.Entry<String, Boolean> entry : boolValues.entrySet()) {

            String  key     = entry.getKey();
            Boolean bool    = entry.getValue();
            Boolean objKeyB = obj.getBoolValues().get(key);

            if (bool != objKeyB) {
                LOGGER.info("The {} ({}) does not match the filter. For " +
                            "'{}' filter, expected value is: {}, current: {}",
                            obj.getClass().getSimpleName(),
                            identifier,
                            key,
                            bool,
                            objKeyB);
                return false;
            }
        }

        for (Map.Entry<String, Integer> entry : intValues.entrySet()) {

            String key = entry.getKey();
            int    val = entry.getValue();
            int    objKeyInt;

            if (key.startsWith(MIN_PREFIX)) {
                objKeyInt = obj.getIntValues().get(MIN_PREFIX + key);
            } else {
                val *= -1;
                objKeyInt = obj.getIntValues().get(MAX_PREFIX + key) * -1;
            }

            if (val > objKeyInt) {
                LOGGER.info("The {} ({}) does not match the filter. For " +
                            "'{}' filter, expected limit is: {}, current: {}",
                            obj.getClass().getSimpleName(),
                            identifier,
                            key,
                            val,
                            objKeyInt);
                return false;
            }
        }

        return true;
    }

    /**
     * TODO describe this method
     */
    public abstract boolean isMatch();
}
