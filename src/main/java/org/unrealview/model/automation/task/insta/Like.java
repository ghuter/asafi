/*
 * Like.java                                     date: 03/05/2020 21:16
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.insta;

import main.java.org.unrealview.model.automation.object.Post;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Like extends HumanTsk {

    public Like() { }

    @Override
    public boolean make() {

        if (current == null || !(current instanceof Post)) { return false; }

        Post    post  = (Post) current;
        boolean liked = interpreter.browser.like(post);

        if (liked) {
            interpreter.newLike();
            LOGGER.info("The post '{}' has been liked.",
                        post.getStringValues().get("shortcode"));
        } else {
            LOGGER.warn("Failed to like post ('{}')",
                        post.getStringValues().get("shortcode"));
        }

        return liked;
    }
}
