/*
 * Property.java                                 date: 08/12/2021 20:35
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation;

import java.util.HashMap;
import java.util.Map;

import static main.java.org.unrealview.model.automation.search.RegexKey.*;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class Property {

    /** TODO describe this attribute */
    public static final String MAX_PREFIX = "maximum ";

    /** TODO describe this attribute */
    public static final String MIN_PREFIX = "minimum ";

    public static final int DEFAULT_NUMERIC_VALUE = -1;

    public static final Map<String, String> USER_INT_VALUES = new HashMap<>() {{
        put("followers amount", USER_FOLLOWERS_AMOUNT);
        put("following amount", USER_FOLLOWING_AMOUNT);
        put("post amount", USER_POST_AMOUNT);
    }};

    public static final Map<String, String> USER_BOOL_VALUES =
            new HashMap<>() {{
                put("is private", USER_PRIVATE_ACCOUNT);
                put("is business", USER_BUSINESS_ACCOUNT);
                put("is verified", USER_VERIFIED_ACCOUNT);
                // put("avatar is setup", USER_NO_PROFILE_PIC);
            }};

    public static final Map<String, String> POST_INT_VALUES = new HashMap<>() {{
        put("like amount", POST_LIKE_AMOUNT);
        put("comment amount", POST_COMMENT_AMOUNT);
    }};

    public static final Map<String, String> POST_BOOL_VALUES =
            new HashMap<>() {{
                put("is video", POST_IS_VIDEO);
                // put("is carousel", POST_IS_CAROUSEL);
                put("comment disabled", POST_COMMENT_DISABLED);
                put("comment disabled for viewer",
                    POST_COMMENT_DISABLED_FOR_YOU);
                put("is paid partnership", POST_IS_PAID_PARTNERSHIP);
                put("is affiliate", POST_IS_AFFILIATE);
                put("viewer has liked", POST_VIEWER_HAS_LIKED);
                put("viewer has saved", POST_VIEWER_HAS_SAVED);
                put("viewer has saved to collection",
                    POST_VIEWER_HAS_SAVED_TO_COLLECTION);
                put("is ad", POST_IS_AD);
            }};

    public static final String OWNER = "owner";

    public static final String LIKES = "likes";

    public static final String FOLLOWERS = "followers";

    public static final String FOLLOWING = "following";

    public static final String POST = "post";

    public static final String USER = "user";

    public static final String TAG = "tag";


    public static final String FIFO = "fifo";

    public static final String LIFO = "lifo";

    public static final String RANDOM = "random";


    private Property() { throw new IllegalStateException("Utility class"); }
}
