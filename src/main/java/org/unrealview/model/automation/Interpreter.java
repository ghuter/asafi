/*
 * Interpreter.java                              date: 20/06/2020 16:09
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation;

import main.java.org.unrealview.model.exception.InternalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Interpreter extends TimerTask {

    private static final Logger LOGGER = LogManager.getLogger("main");

    /** TODO describe this attribute */
    private final Profile profile;

    private final Timer timer;

    /** Quantity of run needed. */
    private int repeat;

    /** Quantity of like performed so far. */
    private int likeAmount;

    /** Quantity of follow performed so far. */
    private int followAmount;

    /** Quantity of block performed so far. */
    private int blockAmount;

    /** Quantity of unfollow performed so far. */
    private int unfollowAmount;

    /** Quantity of comment performed so far. */
    private int commentAmount;

    /** TODO describe this attribute */
    public Pool pool;

    /** The current browser used to perform the tasks. */
    public Browser browser;


    public Interpreter(Pool pool, Timer timer, Profile profile, int repeat) {
        this.pool = pool;
        this.timer = timer;
        this.repeat = repeat;
        this.profile = profile;
    }

    private boolean ckRepeatTsk() {

        if (--repeat == -1) {
            LOGGER.info("Stop profile definitively.");
            timer.cancel();
            timer.purge();
            return true;
        }

        return false;
    }

    @Override
    public void run() {

        LOGGER.info("Starting profile...");

        if (ckRepeatTsk()) { return; }

        browser = new Browser(profile.username, profile.password);

        likeAmount = 0;
        followAmount = 0;
        blockAmount = 0;
        unfollowAmount = 0;
        commentAmount = 0;

        try {
            if (browser.connect()) {
                LOGGER.info("Connected to Instagram ('{}')", profile.username);
            } else {
                LOGGER.warn("Failed to connect to Instagram ('{}')",
                            profile.username);
                return;
            }
        } catch (InternalException e) {
            LOGGER.error("Something wrong with connection to " +
                         "Instagram ('{}')", profile.username);
            LOGGER.debug(e.getMessage());
        }

        profile.tasks.forEach(tsk -> {
            tsk.interpreter = this;
            tsk.execute(null);
        });

        try {
            if (browser.disconnect()) {
                LOGGER.info("Disconnecting to Instagram ('{}')",
                            profile.username);
            } else {
                LOGGER.warn("Failed to disconnect to Instagram ('{}')",
                            profile.username);
            }
        } catch (InternalException e) {
            LOGGER.error("Something wrong with disconnection to " +
                         "Instagram ('{}')", profile.username);
            LOGGER.debug(e.getMessage());
        }

        pool.fileDataContext.writeAll();

        if (ckRepeatTsk()) { return; }

        LOGGER.info("Stop until the next iteration.");
    }

    /**
     * TODO describe this method
     */
    public void newLike() {
        likeAmount++;
        pool.newLike();
    }

    /**
     * TODO describe this method
     */
    public void newFollow() {
        followAmount++;
        pool.newFollow();
    }

    /**
     * TODO describe this method
     */
    public void newComment() {
        commentAmount++;
        pool.newComment();
    }
}
