/*
 * Profile.java                                  date: 03/05/2020 21:05
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation;

import main.java.org.unrealview.model.automation.task.AsafiTsk;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Profile {

    /** The name of the user's account. */
    public String username;

    /** His password for the Instagram login. */
    public String password;

    /** List of comment lists available. */
    public HashMap<String, ArrayList<String>> comments;

    /** TODO describe this attribute */
    public ArrayList<AsafiTsk> tasks;

    public Profile() { }
}
