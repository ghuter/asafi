/*
 * Browser.java                                  date: 06/05/2020 18:37
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation;

import com.roxstudio.utils.CUrl;
import main.java.org.unrealview.model.automation.object.Post;
import main.java.org.unrealview.model.automation.object.User;
import main.java.org.unrealview.model.automation.search.RegexKey;
import main.java.org.unrealview.model.automation.search.UrlKey;
import main.java.org.unrealview.model.exception.InternalException;
import main.java.org.unrealview.model.exception.InvalidRegexException;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static main.java.org.unrealview.model.automation.search.QueryHash.USER_POST;
import static main.java.org.unrealview.model.automation.search.RegexKey.POST_CODE;
import static main.java.org.unrealview.model.automation.search.RegexKey.POST_SHORTCODE;
import static main.java.org.unrealview.model.automation.search.RegexKey.USER_NAME_JSON;
import static main.java.org.unrealview.model.automation.search.UrlKey.INSTAGRAM_POST_USER;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Browser {

    private static final Logger LOGGER = LogManager.getLogger("main");

    /** TODO describe this attribute */
    private final String DEBUG_FMT_FROM_REGEX =
            "%s: can't find pattern.\nregex=\"%s\"\nsrc=\"%s\"";

    private final String USER_AGENT =
            "Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 " +
            "(KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36";

    private final String APP_ID = "x-ig-app-id: 936619743392459";

    /** The name of the user's account. */
    private final String username;

    /** His password for the Instagram login. */
    private final String password;

    private String userId;

    private CUrl.MemIO cookies;

    private String csrftoken;

    private Pattern pattern;

    private Matcher matcher;


    public Browser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    private boolean boolFromRegex(String src, String regex, String truePattern,
                                  String falsePattern) throws InvalidRegexException {
        String  value;
        boolean hfp = falsePattern == null;

        matcher = Pattern.compile(regex).matcher(src);

        if (matcher.find()) {
            value = src.substring(matcher.start(), matcher.end());
        } else {
            if (hfp) { return false; }

            throw new IllegalArgumentException(DEBUG_FMT_FROM_REGEX.formatted(
                    "boolFromRegex",
                    regex,
                    src));
        }

        boolean t = value.equals(truePattern);

        if (!t && !(hfp || value.equals(falsePattern))) {
            throw new InvalidRegexException("Matching result doesn't match" +
                                            " with true or false pattern" +
                                            ".\ntruePattern: " +
                                            "'%s'\nfalsePattern: '%s'".formatted(
                                                    truePattern,
                                                    falsePattern));
        }

        return t;
    }

    private int intFromRegex(String src,
                             String regex) throws InvalidRegexException {

        matcher = Pattern.compile(regex).matcher(src);
        if (matcher.find()) {
            return Integer.parseInt(src.substring(matcher.start(),
                                                  matcher.end()));
        }

        throw new InvalidRegexException(DEBUG_FMT_FROM_REGEX.formatted(
                "intFromRegex",
                regex,
                src));
    }

    private String strFromRegex(String src,
                                String regex) throws InvalidRegexException {

        matcher = Pattern.compile(regex).matcher(src);
        if (matcher.find()) {
            return src.substring(matcher.start(), matcher.end());
        }

        throw new InvalidRegexException(DEBUG_FMT_FROM_REGEX.formatted(
                "strFromRegex",
                regex,
                src));
    }

    private String curlWithCookies(String url) {
        return new CUrl(url).cookie(cookies).exec(CUrl.UTF8, null);
    }

    private List<User> grabUserList(String urlKey,
                                    User user) throws InternalException {

        final int GRAB_AMOUNT = 9000;

        CUrl       curl;
        String     raw;
        String[]   rawSplit;
        List<User> users     = new ArrayList<>();
        int        nextMaxId = 0;

        do {
            curl = new CUrl(urlKey.formatted(user.getStringValues().get("id"),
                                             GRAB_AMOUNT + 1,
                                             nextMaxId)).header(APP_ID)
                                                        .cookie(cookies)
                                                        .opt("--compressed");
            raw = curl.exec(CUrl.UTF8, null);

            if (raw.startsWith("{\"users\":[]")) { return users; }

            rawSplit = raw.split("},\\{");

            try {
                for (String usr : rawSplit) {
                    users.add(new User(strFromRegex(usr, USER_NAME_JSON)));
                }
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get username from json in grab list" +
                             " operation.");
                e.printStackTrace();
                throw new InternalException(e.getMessage());
            }

            try {
                nextMaxId = intFromRegex(raw, RegexKey.NEXT_MAX_ID);
            } catch (Exception e) { break; }
        } while (true);

        return users;
    }

    /**
     * TODO describe this method
     */
    private String actionWithUrl(String scope, String target, String action) {

        CUrl curl = new CUrl(UrlKey.INSTAGRAM_CMD.formatted(scope,
                                                            target,
                                                            action)).header(
                                                                            "x-csrftoken: " + csrftoken)
                                                                    .cookie(cookies)
                                                                    .opt("-X",
                                                                         "POST",
                                                                         "--compressed");

        return curl.exec(CUrl.UTF8, null);
    }

    public boolean pingUrl(String spec) {
        try {
            URL           url  = new URL(spec);
            URLConnection conn = url.openConnection();
            conn.connect();
            conn.getInputStream().close();
            return true;
        } catch (IOException e) { return false; }
    }

    /**
     * TODO describe this method
     */
    public boolean connect() throws InternalException {

        boolean connected;
        CUrl curl = new CUrl(UrlKey.INSTAGRAM_LOGIN).header("user-agent: " +
                                                            USER_AGENT);
        String code = "0";
        cookies = new CUrl.MemIO();

        try {
            csrftoken = strFromRegex(curl.exec(CUrl.UTF8, null),
                                     RegexKey.CSRFTOKEN);
        } catch (InvalidRegexException e) {
            throw new InternalException();
        }

        curl = new CUrl(UrlKey.INSTAGRAM_LOGIN_AJAX).header("x-requested-with" +
                                                            ": XMLHttpRequest")
                                                    .header("user-agent: " +
                                                            USER_AGENT)
                                                    .header("x-csrftoken: " +
                                                            csrftoken)
                                                    .opt("--data-raw",
                                                         ("username=%s" +
                                                          "&enc_password" +
                                                          "=#PWD_INSTAGRAM_BROWSER:%s:%d:%s").formatted(
                                                                 username,
                                                                 code,
                                                                 System.currentTimeMillis() /
                                                                 1000,
                                                                 password))
                                                    .cookieJar(cookies);

        try {
            connected = boolFromRegex(curl.exec(CUrl.UTF8, null),
                                      RegexKey.IS_LOGGED,
                                      "true",
                                      "false");
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get authentification status.");
            throw new InternalException(e.getMessage());
        }

        curl = new CUrl(UrlKey.INSTAGRAM).cookie(cookies);
        try {
            String exec = curl.exec(CUrl.UTF8, null);
            csrftoken = strFromRegex(exec, RegexKey.CSRFTOKEN);
            userId = strFromRegex(exec, RegexKey.VIEWER_ID);
        } catch (InvalidRegexException e) {
            throw new InternalException();
        }

        return connected;
    }

    /**
     * TODO describe this method
     */
    public boolean disconnect() throws InternalException {

        boolean connected;

        CUrl curl = new CUrl(UrlKey.INSTAGRAM_LOGOUT_AJAX).header("x-ig-app" +
                                                                  "-id: " +
                                                                  APP_ID)
                                                          .header("user-agent" +
                                                                  ": " +
                                                                  USER_AGENT)
                                                          .header("x-csrftoken: " +
                                                                  csrftoken)
                                                          .cookie(cookies)
                                                          .opt("--data-raw",
                                                               "user_id=" +
                                                               userId);

        String exec = curl.exec(CUrl.UTF8, null);

        return exec.equals("{\"status\":\"ok\"}");
    }

    ////////////////////////////////////////////////////////////////////////////
    // Operations on the url corresponding to a post.
    ////////////////////////////////////////////////////////////////////////////

    /**
     * TODO describe this method
     */
    public Optional<Post> obtainPost(
            String shortcode) throws InternalException {

        String src =
                curlWithCookies(UrlKey.INSTAGRAM_GET_POST.formatted(shortcode));

        if (src.startsWith("{}")) { return Optional.empty(); }

        Map<String, Boolean> boolValues   = new HashMap<>();
        Map<String, Integer> intValues    = new HashMap<>();
        Map<String, String>  stringValues = new HashMap<>();


        for (Map.Entry<String, String> entry :
                Property.POST_BOOL_VALUES.entrySet()) {
            String key   = entry.getKey();
            String regex = entry.getValue();
            try {
                boolValues.put(key, boolFromRegex(src, regex, "true", "false"));
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get {}.", key);
                throw new InternalException(e.getMessage());
            }
        }

        try {
            boolValues.put("is carrousel",
                           boolFromRegex(src,
                                         RegexKey.POST_IS_CAROUSEL,
                                         RegexKey.POST_IS_CAROUSEL,
                                         null));
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get is carrousel.");
            throw new InternalException(e.getMessage());
        }

        for (Map.Entry<String, String> entry :
                Property.POST_INT_VALUES.entrySet()) {
            String key   = entry.getKey();
            String regex = entry.getValue();
            try {
                intValues.put(key, intFromRegex(src, regex));
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get {}.", key);
                throw new InternalException(e.getMessage());
            }
        }

        Instant publishDate;
        String  ownerUsername;


        try {
            stringValues.put("id", strFromRegex(src, RegexKey.POST_ID));
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get post id.");
            throw new InternalException(e.getMessage());
        }

        try {
            stringValues.put("description",
                             StringEscapeUtils.unescapeJava(strFromRegex(src,
                                                                         RegexKey.POST_DESCRIPTION)));
        } catch (InvalidRegexException ignored) { }

        try {
            stringValues.put("shortcode", strFromRegex(src, POST_SHORTCODE));
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get post shortcode.");
            throw new InternalException(e.getMessage());
        }

        try {
            ownerUsername = strFromRegex(src, RegexKey.POST_OWNER);
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get post owner username.");
            throw new InternalException(e.getMessage());
        }


        try {
            publishDate = Instant.ofEpochSecond(Long.parseLong(strFromRegex(src,
                                                                            RegexKey.POST_TIMESTAMP)));
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get post timestamp.");
            throw new InternalException(e.getMessage());
        }

        return Optional.of(new Post(boolValues,
                                    intValues,
                                    stringValues,
                                    publishDate,
                                    new User(ownerUsername)));
    }

    /**
     * TODO describe this method
     */
    public List<User> grabListLikes(Post post) throws InternalException {
        return null;
    }

    /**
     * TODO describe this method
     */
    public boolean like(Post post) {
        return actionWithUrl("likes",
                             post.getStringValues().get("id"),
                             "like").equals("{\"status\":\"ok\"}");
    }

    /**
     * TODO describe this method
     */
    public boolean unlike(Post post) {

        try {
            return boolFromRegex(actionWithUrl("likes",
                                               post.getStringValues().get("id"),
                                               "unlike"),
                                 RegexKey.STATUS_OK,
                                 "ok",
                                 null);
        } catch (InvalidRegexException e) {
            return false;
        }
    }

    /**
     * TODO describe this method
     */
    public void comment(Post post, String text) {
        CUrl curl = new CUrl(UrlKey.INSTAGRAM_CMD.formatted("comments",
                                                            post.getStringValues()
                                                                .get("id"),
                                                            "add")).header(
                                                                           "x-csrftoken: " + csrftoken)
                                                                   .cookie(cookies)
                                                                   .opt("--data-raw",
                                                                        ("comment_text=%s&replied_to_comment_id=%s").formatted(
                                                                                text,
                                                                                ""),
                                                                        "-X",
                                                                        "POST",
                                                                        "--compressed");
    }

    /**
     * TODO describe this method
     */
    public void save(Post post) {

    }

    /**
     * TODO describe this method
     */
    public void unsave(Post post) {

    }

    ////////////////////////////////////////////////////////////////////////////
    // Operations on the url corresponding to a user.
    ////////////////////////////////////////////////////////////////////////////

    /**
     * TODO describe this method
     */
    public Optional<User> obtainUser(String username) throws InternalException {

        String src =
                curlWithCookies(UrlKey.INSTAGRAM_GET_USER.formatted(username));

        if (src.startsWith("{}")) { return Optional.empty(); }

        Map<String, Boolean> boolValues   = new HashMap<>();
        Map<String, Integer> intValues    = new HashMap<>();
        Map<String, String>  stringValues = new HashMap<>();

        for (Map.Entry<String, String> entry :
                Property.USER_BOOL_VALUES.entrySet()) {
            String key   = entry.getKey();
            String regex = entry.getValue();
            try {
                boolValues.put(key, boolFromRegex(src, regex, "true", "false"));
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get {}.", key);
                throw new InternalException(e.getMessage());
            }
        }

        try {
            boolValues.put("avatar is setup",
                           !boolFromRegex(src,
                                          RegexKey.USER_NO_PROFILE_PIC,
                                          RegexKey.USER_NO_PROFILE_PIC,
                                          null));
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get avatar is setup.");
            throw new InternalException(e.getMessage());
        }

        for (Map.Entry<String, String> entry :
                Property.USER_INT_VALUES.entrySet()) {
            String key   = entry.getKey();
            String regex = entry.getValue();
            try {
                intValues.put(key, intFromRegex(src, regex));
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get {}.", key);
                throw new InternalException(e.getMessage());
            }
        }

        try {
            stringValues.put("description",
                             StringEscapeUtils.unescapeJava(strFromRegex(src,
                                                                         RegexKey.USER_DESCRIPTION)));
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get user description.");
            throw new InternalException(e.getMessage());
        }

        try {
            stringValues.put("id", strFromRegex(src, RegexKey.USER_ID));
        } catch (InvalidRegexException e) {
            LOGGER.error("Failed to get user id.");
            throw new InternalException(e.getMessage());
        }

        stringValues.put("username", username);

        return Optional.of(new User(boolValues, intValues, stringValues));
    }

    /**
     * TODO describe this method
     */
    public List<User> grabListFollowers(User user) throws InternalException {

        List<User> users = grabUserList(UrlKey.FOLLOWERS, user);

        if (users.size() == 0) {
            LOGGER.warn("Probably '{}' can't access to private account '{}'. " +
                        "Followers list is empty.",
                        username,
                        user.getStringValues().get("username"));
            return users;
        }

        LOGGER.info("Followers list of '{}' have been grabbed.",
                    user.getStringValues().get("username"));
        return users;
    }

    /**
     * TODO describe this method
     */
    public List<User> grabListFollowing(User user) throws InternalException {

        List<User> users = grabUserList(UrlKey.FOLLOWING, user);

        if (users.size() == 0) {
            LOGGER.warn("Probably '{}' can't access to private account '{}'. " +
                        "Following list is empty.",
                        username,
                        user.getStringValues().get("username"));
            return users;
        }

        LOGGER.info("Following list of '{}' have been grabbed.",
                    user.getStringValues().get("username"));

        return users;
    }

    /**
     * TODO describe this method
     *
     * @return
     */
    public List<Post> grabListPostTimeline(
            int amount) throws InternalException {

        CUrl       curl;
        String     nextMaxId = "";
        String     raw;
        String[]   rawSplit;
        List<Post> posts     = new ArrayList<>();

        do {
            curl = new CUrl(UrlKey.TIMELINE).header(APP_ID)
                                            .cookie(cookies)
                                            .opt("--data-raw",
                                                 "max_id=" + nextMaxId,
                                                 "--compressed");
            raw = curl.exec(CUrl.UTF8, null);

            if (raw.startsWith("{\"num_results\":0")) { return posts; }

            rawSplit = raw.split("like_and_view_counts_disabled");

            try {
                for (int i = 0; i < rawSplit.length - 1; i++) {
                    String shtCo = rawSplit[i];
                    posts.add(new Post(strFromRegex(shtCo, POST_CODE)));
                }
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get username from json in grab list" +
                             " operation.");
                throw new InternalException(e.getMessage());
            }

            try {
                nextMaxId = strFromRegex(raw, RegexKey.MAX_ID);
            } catch (Exception e) { break; }
        } while (posts.size() < amount);

        IntStream.iterate(posts.size(), i -> i > amount, i -> i - 1)
                 .map(i -> amount)
                 .forEachOrdered(posts::remove);

        return posts;
    }

    /**
     * TODO describe this method
     *
     * @return
     */
    public List<Post> grabListPostOfUser(User user,
                                         int amount) throws InternalException {

        final int GRAB_AMOUNT = 50;

        CUrl       curl;
        String     end_c = "";
        String     raw;
        String[]   rawSplit;
        List<Post> posts = new ArrayList<>();

        do {
            curl = new CUrl(INSTAGRAM_POST_USER.formatted(USER_POST,
                                                          user.getStringValues()
                                                              .get("id"),
                                                          GRAB_AMOUNT,
                                                          end_c));

            raw = curl.exec(CUrl.UTF8, null);
            rawSplit = raw.split("taken_at_timestamp");

            try {
                for (int i = 0; i < rawSplit.length - 1; i++) {
                    String shtCo = rawSplit[i];
                    posts.add(new Post(strFromRegex(shtCo, POST_SHORTCODE)));
                }
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get post from json in grab list" +
                             " operation.");
                throw new InternalException(e.getMessage());
            }

            try {
                end_c = strFromRegex(rawSplit[0], RegexKey.END_CURSOR);
            } catch (Exception e) { break; }
            if (end_c.isEmpty()) { break; }
        } while (posts.size() < amount);

        IntStream.iterate(posts.size(), i -> i > amount, i -> i - 1)
                 .map(i -> amount)
                 .forEachOrdered(posts::remove);

        return posts;
    }

    /**
     * TODO describe this method
     */
    public void follow() {

    }

    /**
     * TODO describe this method
     */
    public void unfollow() {

    }

    /**
     * TODO describe this method
     */
    public void block() {

    }

    ////////////////////////////////////////////////////////////////////////////
    // Operations on the url corresponding to a tag.
    ////////////////////////////////////////////////////////////////////////////

    /**
     * TODO describe this method
     */
    public List<Post> grabListPostOfTag(String tagName,
                                        int amount) throws InternalException {

        CUrl       curl;
        String     nextMaxId = "";
        String     raw;
        String[]   rawSplit;
        List<Post> posts     = new ArrayList<>();

        do {
            curl =
                    new CUrl(UrlKey.INSTAGRAM_SECTION_TAG.formatted(tagName)).header(
                                                                                     APP_ID)
                                                                             .cookie(cookies)
                                                                             .opt("--data-raw",
                                                                                  "max_id=" +
                                                                                  nextMaxId,
                                                                                  "--compressed");
            raw = curl.exec(CUrl.UTF8, null);

            if (raw.startsWith("{\"num_results\":0")) { return posts; }

            rawSplit = raw.split("like_and_view_counts_disabled");

            try {
                for (int i = 0; i < rawSplit.length - 1; i++) {
                    String shtCo = rawSplit[i];
                    posts.add(new Post(strFromRegex(shtCo, POST_CODE)));
                }
            } catch (InvalidRegexException e) {
                LOGGER.error("Failed to get username from json in grab list" +
                             " operation.");
                throw new InternalException(e.getMessage());
            }

            try {
                nextMaxId = strFromRegex(raw, RegexKey.MAX_ID);
            } catch (Exception e) { break; }
        } while (posts.size() < amount);

        IntStream.iterate(posts.size(), i -> i > amount, i -> i - 1)
                 .map(i -> amount)
                 .forEachOrdered(posts::remove);

        return posts;
    }
}