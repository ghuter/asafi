package main.java.org.unrealview.model.automation;

import main.java.org.unrealview.model.GlobalSettings;
import main.java.org.unrealview.model.file.FileDataContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Timer;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Pool {

    private static final Logger LOGGER = LogManager.getLogger("main");

    /** TODO describe this attribute */
    private final GlobalSettings settings;

    /** TODO describe this attribute */
    private final ArrayList<Timer> timers;


    /** TODO describe this attribute */
    private final Instant begin;

    /** TODO describe this attribute */
    public final FileDataContext fileDataContext;

    /** TODO describe this attribute */
    private int likeAmount;

    /** TODO describe this attribute */
    private int followAmount;

    /** TODO describe this attribute */
    private int commentAmount;


    public Pool(GlobalSettings settings, FileDataContext fileDataContext) {

        LOGGER.info("The pool init timer profile.");

        this.settings = settings;
        this.fileDataContext = fileDataContext;

        timers = new ArrayList<>();
        begin = Instant.now();

        settings.profilePlans.forEach(plan -> {

            Timer timer = new Timer(plan.name);

            timers.add(timer);
            timer.schedule(new Interpreter(this,timer, plan.profile,
                                           plan.repeat),
                           plan.delay * 1000,
                           plan.period * 1000);
        });

        LOGGER.trace("The pool for profile now exited.");
    }

    private void ckLimit(int amount, int maxAmount) {

        long elapsed = ChronoUnit.SECONDS.between(Instant.now(), begin);

        if (elapsed > 3600) {
            likeAmount = followAmount = commentAmount = 0;
            begin.plus(1, ChronoUnit.HOURS);
            return;
        }

        long wait = (amount / maxAmount) * 3600L - elapsed;

        if (wait > 0) {
            try {
                LOGGER.info("Waiting for respect global limit: {}s - max " +
                            "amount: {}", wait, maxAmount);
                Thread.sleep(wait);
            } catch (InterruptedException ignored) { }
        }
    }

    /**
     * TODO describe this method
     */
    protected void newLike() { ckLimit(++likeAmount, settings.likeMaxAmount); }

    /**
     * TODO describe this method
     */
    protected void newFollow() {
        ckLimit(++followAmount, settings.followMaxAmount);
    }

    /**
     * TODO describe this method
     */
    protected void newComment() {
        ckLimit(++commentAmount, settings.commentsMaxAmount);
    }
}
