/*
 * UrlKey.java                                   date: 05/05/2020 21:28
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.search;

/**
 * URLs provide access to the various Instagram pages.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class UrlKey {

    /** URL of site to perform the internet access test. */
    public static final String INTERNET = "https://wiki.archlinux.org";

    /** Instagram URL to perform the availability test and login. */
    public static final String INSTAGRAM = "https://www.instagram.com/";

    public static final String INSTAGRAM_LOGIN =
            "https://www.instagram.com/accounts/login/";

    /** TODO describe this attribute */
    public static final String INSTAGRAM_LOGIN_AJAX =
            "https://www.instagram.com/accounts/login/ajax/";

    /** TODO describe this attribute */
    public static final String INSTAGRAM_LOGOUT_AJAX =
            "https://www.instagram.com/accounts/logout/ajax/";

    /** Instagram URL to get shared data for password encryption. */
    public static final String INSTAGRAM_SHARED =
            "https://www.instagram.com/data/shared_data";

    /** User access URL. */
    public static final String INSTAGRAM_USER = INSTAGRAM + "%s";

    /** URL for access to user information. */
    public static final String INSTAGRAM_GET_USER = INSTAGRAM + "%s/?__a=1";

    /** URL for access to post from tag. */
    public static final String INSTAGRAM_SECTION_TAG =
            "https://i.instagram.com/api/v1/tags/%s/sections/";

    /** Tag access URL. */
    public static final String INSTAGRAM_TAG = INSTAGRAM + "explore/tags/";

    /** URL for access to publications. */
    public static final String INSTAGRAM_POST = INSTAGRAM + "p/";

    /** URL for access to publication information. */
    public static final String INSTAGRAM_GET_POST =
            INSTAGRAM_POST + "%s/?__a=1";

    /** URL for execute command (type/cible/action). */
    public static final String INSTAGRAM_CMD = INSTAGRAM + "web/%s/%s/%s/";

    /** URL for request with query_hash parameter. */
    public static final String INSTAGRAM_HASH_REQUEST =
            INSTAGRAM + "graphql/query/?query_hash=%s";

    /**
     * URL for request with query_hash parameter (user id/amount/end cursor)
     * .
     */
    public static final String INSTAGRAM_POST_USER = INSTAGRAM_HASH_REQUEST +
                                                     "&variables={\"id\":\"%s" +
                                                     "\",\"first\":%d," +
                                                     "\"after\":\"%s\"}";

    /** URL for grab followers list. */
    public static final String FOLLOWERS = "https://i.instagram" +
                                           ".com/api/v1/friendships/%s" +
                                           "/followers/?count=%d&max_id=%d" +
                                           "&search_surface=follow_list_page";

    /** URL for grab following list. */
    public static final String FOLLOWING = "https://i.instagram" +
                                           ".com/api/v1/friendships/%s" +
                                           "/following/?count=%d&max_id=%d" +
                                           "&search_surface=follow_list_page";

    /** URL for grab timeline posts list. */
    public static final String TIMELINE  =
            "https://i.instagram.com/api/v1/feed/timeline/";

    private UrlKey() { throw new IllegalStateException("Utility class"); }
}
