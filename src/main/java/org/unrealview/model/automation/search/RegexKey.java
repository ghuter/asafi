/*
 * RegexKey.java                                 date: 05/05/2020 21:26
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */


package main.java.org.unrealview.model.automation.search;

/**
 * Regex to target different information in the source code of the pages.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class RegexKey {

    /** TODO describe this attribute */
    public static final String CSRFTOKEN =
            "(?<=\"csrf_token\":\").*(?=\",\"viewer\")";

    /** TODO describe this attribute */
    public static final String VIEWER_ID =
            "(?<=\"viewerId\":\")\\d*(?=\"})";

    /** TODO describe this attribute */
    public static final String IS_LOGGED =
            "(?<=\"authenticated\":).{4,5}(?=,\"oneTapPrompt\")";

    /** TODO describe this attribute */
    public static final String INSTAGRAM_USERNAME = "[0-9a-z_\\.]{1,30}";

    /** TODO describe this attribute */
    public static final String END_CURSOR = "(?<=\"end_cursor\":\").*?(?=\"},)";

    /** TODO describe this attribute */
    public static final String STATUS_OK = "(?<=\"status\":\").*?(?=\"})";

    /** TODO describe this attribute */
    public static final String MAX_ID = "(?<=\"next_max_id\":\").{100,200}" +
                                        "(?=\",\"view_state_version\":\")";

    ////////////////////////////////////////////////////////////////////////////
    // Regex useful to find information about the user.
    ////////////////////////////////////////////////////////////////////////////

    /** Regex to get the id of an account. */
    public static final String USER_ID = "(?<=\"id\":\")\\d+(?=\",\")";

    /** Regex to get the username of an account from html. */
    public static final String USER_NAME_HTML = "(?<=,\"username\":\")" +
                                                INSTAGRAM_USERNAME +
                                                "(?=\",\"connected_fb_page\":)";

    /** Regex to get the username of an account from json. */
    public static final String USER_NAME_JSON = "(?<=,\"username\":\")" +
                                                INSTAGRAM_USERNAME +
                                                "(?=\",\"full_name\":)";

    /** Regex to determine if you're following the account. */
    public static final String USER_FOLLOWED_BY_YOU =
            "\"followed_by_viewer\":true";

    /** Regex to determine if you block the account. */
    public static final String USER_BLOCKED_BY_YOU =
            "\"blocked_by_viewer\":true";

    /** Regex to determine if the account is blocking you. */
    public static final String USER_BLOCK_YOU = "\"has_blocked_viewer\":true";

    /** Regex to determine the amount of accounts followed. */
    public static final String USER_FOLLOWERS_AMOUNT =
            "(?<=\"edge_followed_by\":\\{\"count\":)\\d+";

    /** Regex to determine the amount of accounts that follow. */
    public static final String USER_FOLLOWING_AMOUNT =
            "(?<=\"edge_follow\":\\{\"count\":)\\d+";

    /** Regex to determine the amount of publications in an account. */
    public static final String USER_POST_AMOUNT =
            "(?<=\"edge_owner_to_timeline_media\":\\{\"count\":)\\d+";

    /** Regex to get the description of an account. */
    public static final String USER_DESCRIPTION =
            "(?<=\"user\":\\{\"biography\":\").*?(?=\",\"blocked_by_viewer\":)";

    /** Regex to determine if it's a business account. */
    public static final String USER_BUSINESS_ACCOUNT =
            "(?<=\"is_business_account\":).{4,5}(?=," +
            "\"is_professional_account\":)";

    /** Regex to determine if it's a verified account. */
    public static final String USER_VERIFIED_ACCOUNT =
            "(?<=\"is_verified\":).{4,5}(?=,\"edge_mutual_followed_by\":)";

    /** Regex to determine if it's a private account. */
    public static final String USER_PRIVATE_ACCOUNT =
            "(?<=\"is_private\":).{4,5}(?=,\"is_verified\":)";

    /** Regex to determine the account has a profile image set up. */
    public static final String USER_NO_PROFILE_PIC =
            "44884218_345707102882519_2446069589734326272_n";

    /** Regex to determine if this account is requested by viewer. */
    public static final String USER_IS_REQUESTED =
            "(?<=\"requested_by_viewer\":).{4,5}(?=,\"should_show_category\":)";

    /** TODO describe this attribute */
    public static final String NEXT_MAX_ID =
            "(?<=\"next_max_id\":\")\\d*(?=\",\"status\":)";

    ////////////////////////////////////////////////////////////////////////////
    // Regex useful to find information about the post.
    ////////////////////////////////////////////////////////////////////////////

    /** TODO describe this attribute */
    public static final String POST_ID =
            "(?<=\"id\":\")\\d+(?=\",\"shortcode\")";

    /** TODO describe this attribute */
    public static final String POST_SHORTCODE =
            "(?<=\"shortcode\":\").*?(?=\",)";

    /** Regex to get shortcode with just code prefix. */
    public static final String POST_CODE =
            "(?<=\"code\":\").*?(?=\",)";

    /** Regex to get the description of a publication. */
    public static final String POST_DESCRIPTION =
            "(?<=\"text\":\").+(?=\"}}]})";

    /** Regex to get the description of a publication. */
    public static final String POST_OWNER = "(?<=\"username\":\")" +
                                            INSTAGRAM_USERNAME +
                                            "(?=\",\"blocked_by_viewer\")";

    /** Regex to determine the like amount of publications. */
    public static final String POST_LIKE_AMOUNT =
            "(?<=\"edge_media_preview_like\":\\{\"count\":)\\d+";

    /** Regex to determine the comments amount of publications. */
    public static final String POST_COMMENT_AMOUNT =
            "(?<=\"edge_media_to_parent_comment\":\\{\"count\":)\\d+";

    /** Regex to determine if the publication is a video. */
    public static final String POST_IS_VIDEO =
            "(?<=\"is_video\":).{4,5}(?=,\"tracking_token\":)";

    /** Regex to determine if the publication is a carousel. */
    public static final String POST_IS_CAROUSEL = "edge_sidecar_to_children";

    /** Regex to determine if comments are disabled. */
    public static final String POST_COMMENT_DISABLED =
            "(?<=\"comments_disabled\":).{4,5}(?=," +
            "\"commenting_disabled_for_viewer\":)";

    /** Regex to determine if comments are disabled for the current account. */
    public static final String POST_COMMENT_DISABLED_FOR_YOU =
            "(?<=\"commenting_disabled_for_viewer\":).{4,5}(?=," +
            "\"taken_at_timestamp\":)";

    /** Regex to determine if comments is paid partnership. */
    public static final String POST_IS_PAID_PARTNERSHIP =
            "(?<=\"is_paid_partnership\":).{4,5}(?=,\"location\":)";

    /** TODO describe this attribute */
    public static final String POST_IS_AFFILIATE =
            "(?<=\"is_affiliate\":).{4,5}(?=,\"is_paid_partnership\":)";

    /** TODO describe this attribute */
    public static final String POST_VIEWER_HAS_LIKED =
            "(?<=\"viewer_has_liked\":).{4,5}(?=,\"viewer_has_saved\":)";

    /** TODO describe this attribute */
    public static final String POST_VIEWER_HAS_SAVED =
            "(?<=\"viewer_has_saved\":).{4,5}(?=," +
            "\"viewer_has_saved_to_collection\":)";

    /** TODO describe this attribute */
    public static final String POST_VIEWER_HAS_SAVED_TO_COLLECTION =
            "(?<=\"viewer_has_saved_to_collection\":).{4,5}(?=," +
            "\"viewer_in_photo_of_you\":)";

    /** TODO describe this attribute */
    public static final String POST_VIEWER_CAN_RESHARE =
            "(?<=\"viewer_can_reshare\":).{4,5}(?=,\"owner\":)";

    /** TODO describe this attribute */
    public static final String POST_IS_AD =
            "(?<=\"is_ad\":).{4,5}(?=,\"edge_web_media_to_related_media\":)";

    /** Regex to get timestamps of a publication. */
    public static final String POST_TIMESTAMP =
            "(?<=\"taken_at_timestamp\":)\\d+(?=,\"edge_media_preview_like\")";

    public RegexKey() { throw new IllegalStateException("Utility class"); }
}
