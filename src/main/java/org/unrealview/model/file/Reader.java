/*
 * Reader.java                                   date: 03/05/2020 21:07
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.file;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import main.java.org.unrealview.model.GlobalSettings;
import main.java.org.unrealview.model.automation.Profile;
import main.java.org.unrealview.model.automation.object.Post;
import main.java.org.unrealview.model.automation.object.User;
import main.java.org.unrealview.model.automation.task.browse.BrowseFollowersOfUser;
import main.java.org.unrealview.model.automation.task.browse.BrowseFollowingOfUser;
import main.java.org.unrealview.model.automation.task.browse.BrowseLstFromFile;
import main.java.org.unrealview.model.automation.task.filter.PostFilter;
import main.java.org.unrealview.model.automation.task.filter.UserInLst;
import main.java.org.unrealview.model.automation.task.insta.HumanTsk;
import main.java.org.unrealview.model.automation.task.insta.Like;
import main.java.org.unrealview.model.automation.task.insta.SaveAsList;
import main.java.org.unrealview.model.automation.task.insta.SaveObOUser;
import main.java.org.unrealview.model.schedule.ProfilePlan;

import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static main.java.org.unrealview.model.file.FilePath.*;

/**
 * Set of methods to allow the reading of the different files of the
 * application.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class Reader {

    /**
     * Attempt to read the global settings file.
     *
     * @return the {@link GlobalSettings} object read from file.
     */
    public static GlobalSettings readSettings() throws Exception {

        YamlReader reader = new YamlReader(new FileReader(root +
                                                          sep +
                                                          ROOT +
                                                          sep +
                                                          SETTINGS));
        reader.getConfig().setClassTag("Settings", GlobalSettings.class);
        reader.getConfig().setClassTag("profilePlan", ProfilePlan.class);
        GlobalSettings globalSettings = (GlobalSettings) reader.read();
        reader.close();

        return globalSettings;
    }

    /**
     * TODO describe this method
     */
    public static Map<String, Profile> readProfiles() throws Exception {

        Map<String, Profile> profiles = new HashMap<>();

        YamlConfig c = new YamlConfig();
        c.setClassTag("Profile", Profile.class);
        c.setClassTag("BrowseFollowingOfUser", BrowseFollowingOfUser.class);
        c.setClassTag("BrowseFollowersOfUser", BrowseFollowersOfUser.class);
        c.setClassTag("BrowseListFromFile", BrowseLstFromFile.class);
        c.setClassTag("SaveOneByOneUser", SaveObOUser.class);
        c.setClassTag("SaveAsList", SaveAsList.class);
        c.setClassTag("Like", Like.class);
        c.setClassTag("PostFilter", PostFilter.class);
        c.setClassTag("UserInList", UserInLst.class);
        c.setClassTag("HumanTsk", HumanTsk.class);

        Stream<Path> walk =
                Files.walk(Path.of(root + sep + ROOT + sep + PROFILES));

        for (Path file : walk.filter(Files::isRegularFile)
                             .filter(p -> p.getFileName()
                                           .toString()
                                           .endsWith(YML_EXT))
                             .collect(Collectors.toList())) {
            YamlReader reader =
                    new YamlReader(new FileReader(file.toString()), c);
            Profile profile = (Profile) reader.read();
            profiles.put(file.getFileName().toString(), profile);
        }

        walk.close();

        return profiles;
    }

    /**
     * TODO describe this method
     */
    public static FileDataContext readData() throws Exception {

        FileDataContext fdc = new FileDataContext();

        Path pathByList =
                Path.of(root + sep + ROOT + sep + DATA + sep + BY_LIST);

        Stream<Path> walkDates = Files.walk(pathByList, 1);

        HashMap<Instant, HashMap<String, ArrayList<User>>> usersByLists =
                new HashMap<>();

        for (Path dirDay : walkDates.filter(Files::isDirectory)
                                    .collect(Collectors.toList())) {

            if (dirDay.equals(pathByList)) { continue; }

            Stream<Path> walkDay = Files.walk(dirDay);

            HashMap<String, ArrayList<User>> tmpHM = new HashMap<>();

            for (Path file : walkDay.filter(Files::isRegularFile)
                                    .filter(p -> p.getFileName()
                                                  .toString()
                                                  .endsWith(TSV_EXT))
                                    .collect(Collectors.toList())) {

                String name = file.getFileName().toString();
                tmpHM.put(name.substring(0, name.length() - TSV_EXT.length()),
                          Files.readAllLines(file)
                               .stream()
                               .map(User::new)
                               .collect(Collectors.toCollection(ArrayList::new)));
            }

            usersByLists.put(Instant.parse(dirDay.getFileName().toString()),
                             tmpHM);
        }

        fdc.setUsersByLists(usersByLists);

        Path pathObO =
                Path.of(root + sep + ROOT + sep + DATA + sep + ONE_BY_ONE);

        Stream<Path> walkUsers =
                Files.walk(Path.of(pathObO + sep + "users"), 1);


        HashMap<String, HashMap<Instant, User>> usersOneByOne = new HashMap<>();

        for (Path file : walkUsers.filter(Files::isRegularFile)
                                  .filter(p -> p.getFileName()
                                                .toString()
                                                .endsWith(TSV_EXT))
                                  .collect(Collectors.toList())) {

            HashMap<Instant, User> tmpHM = new HashMap<>();

            Files.readAllLines(file).forEach(s -> {
                String[] split = s.split(TSV_SEP);
                tmpHM.put(Instant.parse(split[0]), new User(split[1]));
            });

            String name = file.getFileName().toString();

            usersOneByOne.put(name.substring(0,
                                             name.length() - TSV_EXT.length()),
                              tmpHM);
        }

        fdc.setUsersOneByOne(usersOneByOne);

        Stream<Path> walkPosts =
                Files.walk(Path.of(pathObO + sep + "posts"), 1);
        HashMap<String, HashMap<Instant, Post>> postsOneByOne = new HashMap<>();

        for (Path file : walkPosts.filter(Files::isRegularFile)
                                  .filter(p -> p.getFileName()
                                                .toString()
                                                .endsWith(TSV_EXT))
                                  .collect(Collectors.toList())) {

            HashMap<Instant, Post> tmpHM = new HashMap<>();

            Files.readAllLines(file).forEach(s -> {
                String[] split = s.split(TSV_SEP);
                tmpHM.put(Instant.parse(split[0]), new Post(split[1]));
            });

            String name = file.getFileName().toString();

            postsOneByOne.put(name.substring(0,
                                             name.length() - TSV_EXT.length()),
                              tmpHM);
        }

        fdc.setPostsOneByOne(postsOneByOne);

        return fdc;
    }
}
