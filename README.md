# ASAFI

> ASAFI (V0.0.89) - Automation, Scrapping, Analyzer For Instagram

Ce logiciel a pour but de fournir des outils pour automatiser des tâches et pour faire de la
récupération d'informations sur Instagram. Pour cela il s'appuie sur des requêtes CURL pour
exploiter l'API d'intagram directement.

- [ASAFI](#asafi)
  - [Présentation](#présentation)
    - [Principe général](#principe-général)
    - [Gestion des données](#gestion-des-données)
      - [Structure des fichiers](#structure-des-fichiers)
      - [Explication de l'organisation](#explication-de-lorganisation)
    - [Informations récupérer](#informations-récupérer)
  - [Les profiles](#les-profiles)
    - [HumanTsk](#humantsk)
      - [Like](#like)
      - [Unlike](#unlike)
      - [Follow](#follow)
      - [Unfollow](#unfollow)
      - [Block](#block)
      - [Unblock](#unblock)
      - [GetOwnerFromPost](#getownerfrompost)
      - [GetOwnerFromComment](#getownerfromcomment)
      - [Limiter](#limiter)
      - [ResetLimiter](#resetlimiter)
      - [SaveAsList](#saveaslist)
      - [SaveOneByOneUser](#saveonebyoneuser)
      - [SaveOneByOnePost](#saveonebyonepost)
    - [BrowseTsk](#browsetsk)
      - [BrowseComment](#browsecomment)
      - [BrowseFollowingOfUser](#browsefollowingofuser)
      - [BrowseFollowersOfUser](#browsefollowersofuser)
      - [BrowseListFromFile](#browselistfromfile)
      - [BrowseOneByOneUser](#browseonebyoneuser)
      - [BrowseOneByOnePost](#browseonebyonepost)
      - [BrowsePostFromTag](#browsepostfromtag)
      - [BrowsePostFromUser](#browsepostfromuser)
    - [FilterTsk](#filtertsk)
      - [InstaObjFilter](#instaobjfilter)
        - [PostFilter](#postfilter)
        - [PostFilter](#postfilter-1)
      - [ListFilter](#listfilter)
        - [UserInList](#userinlist)
        - [PostInList](#postinlist)
  - [Exemples](#exemples)
    - [like_by_tag](#like_by_tag)

## Présentation

### Principe général

L'application s'appuie sur un fichier de configuration générale, des profiles et des fichiers de
données. Les profiles permettent de mettre en relations les différentes tâches disponible entre
elles et de les paramétrer. Pour protéger le compte utilisé, les principales actions (like, comment,
follow) sont compter pour vérifier leur quantité ne dépasse pas les limites générales définit dans
la configuration.

### Gestion des données

#### Structure des fichiers

```tree
config
    global_settings.yml
    +---logs
    +---profiles
    |   follow_like_by_tag.yml
    |   unfollow_unfollowers.yml
    |   block_unfollowers.yml
    |   like_by_feed.yml
    \---data
        +---by_list
        |   +---2021-01-01T00:00:00Z
        |   |   followers_userA.tsv
        |   |   following_userA.tsv
        |   |   followers_userB.tsv
        |   |   likes_AbC1xYz23Ml.tsv
        |   |   likes_AbC2xYz23Ml.tsv
        |   \---2021-01-02T00:00:00Z
        |       followers_userA.tsv
        |       followers_userB.tsv
        |       likes_AbC1xYz23Ml.tsv
        |       likes_AbC2xYz23Ml.tsv
        +---one_by_one
        |   +---users
        |   |   [CustomName1].tsv
        |   \---posts
        |       [CustomName2].tsv
        \---statistics
            user1.tsv
            AbC1xYz23Ml.tsv
```

#### Explication de l'organisation

Le dossier **config** regroupe l'ensemble des dossiers et fichiers de l'application. Son chemin s'il
n'est pas au niveau de l'exécutable devra être précisé en argument.

- *global_settings.yml* : Paramétrage général lu au démarrage de l'application. À priori, indique la
  fréquence de lancement des profiles. Ainsi il n'y aura pas d'options sur la ligne de commande.
  Potentiellement les opérations d'analyse statistiques (non prévue dans un premier temps) se feront
  par un autre point d'entré.
- **logs** : Non choisie encore entre logging rotatifs et logging journalier par exemple. Les logs
  permettront de suivre l'activitée en cours au niveau INFO.
- **profiles** : Liste de fichiers de paramétrage des profiles d'exécutions.
- **data** : Structure de dossiers et de fichiers pour enregistrer et charger des données relatives
  à Instagram générer par les différentes tâches disponibles.

  - **by_list** :
    Le système by_list est conçu pour manipuler des listes complètes avec une résolution temporelles
    de un jour. L'objectif est par exemple et de pouvoir comparer des ensembles de même source d'un
    jour sur l'autre mais aussi éventuellement de pouvoir faire des analyse ultérieurement.

    Le nom de la liste (et du fichier) suis un format simple : **category_name** qui permet de le
    trier et de le retrouver facilement.

    Le nom des dossiers est la date du jour d'enregistrement ; il ne peux pas y avoir
    d'enregistrement multiple : le fichier est écrasé en cas de nouvel enregistrement.

    ```csv
    user1
    user2
    ```

    Le fichier contient la liste des objets représentés sur une ligne par leur version courte (
    username pour un User et shortcode pour un Post).

  - **one_by_one** :
    Ce système permet comme son nom l'indique d'interéagir avec les objets un par un. Il existe
    qu'un seul fichier du même nom et chaque ligne comporte le timestamp d'ajout et la version
    courte de l'objet enregistré. Il est possible de charger une séquence d'objet selon le temps
    depuis lequel il a été ajouté.

    ```csv
    01-12-2020 12:00:00   user1
    01-12-2020 12:00:10   user2
    ```

  - **statistics** :
    Les fichiers de statistiques permettent de suivre l'évolutions des variables importantes d'un
    objets.

    ```csv
    timestamp   post amount  following   followers
    01-12-2020 12:00:00   1   0   0
    02-12-2020 12:00:00   1   1   5
    ```

### Informations récupérer

> Informations qui pourront être extraites pour être utilisées au sein de l'application à des
> fins d'analyse ou pour créer des règles dans les profils d'automatisation.

- **Sur les publications :**
  - Nombre de like
  - Liste de like
  - Nombre de commentaires
  - Liste de commentaires
  - Est une vidéo
  - Est un carrousel
  - Date et heure de publication
  - Texte de la description

- **Sur les profils :**
  - Nombre de publications
  - Nombre d'abonnés
  - Nombre d'abonnements
  - Liste des abonnés
  - Liste des abonnements
  - Est privé
  - Est business
  - Est vérifié
  - Texte de la description

## Les profiles

> Les profiles représentent un ensemble de tâches pour permettre des opérations spécifiques et
> les identifiants du compte à utiliser.
> Pour cela on peut combiner plusieurs types de tâches comme les outils de recherches (pour
> obtenir des objets), des filtres et des tâches à appliquer sur les résultats de ces
> filtres.

Ce système est modulaire, on peux considérer chaque tâches comme un nœud dans un système nodale qui
reçoit un objet à traiter et exécute ensuite ses propres nœuds.

Ainsi de manière générale chaque tâches :

- Est une *AsafiTsk* et ainsi :
- Possède une valeur courante
- A un méchanisme d'exécution

Voici la liste des tâches :

- *HumanTsk*
  - Like
  - Unlike
  - Follow
  - Unfollow
  - Block
  - Unblock
  - GetOwnerFromPost
  - GetOwnerFromComment
  - Limiter
  - ResetLimiter
  - SaveAsList
  - SaveAsObOUser
  - SaveAsObOPost
- *BrowseTsk*
  - BrowseComment
  - BrowseFollowersOfUser
  - BrowseFollowingOfUser
  - BrowseListFromFile
  - BrowseOneByOneUser
  - BrowseOneByOnePost
  - BrowsePostFromTag
  - BrowsePostFromUser
- *FilterTsk*
  - *InstaObjFilter*
    - PostFilter
    - UserFilter
    - CommentFilter
  - *ListFilter*
    - UserInList
    - PostInList

### HumanTsk

- *Paramètres :*
  - **minWaitTime** : Temps d'attente minimal en secondes. (optional, default = 0)
  - **maxWaitTime** : Temps d'attente maximal en secondes. (optional, default = 0)
  - **probability** : Probilité d'exécution de la tâche. (optional, default = 1.0)
  - **tasks** : Listes de tâches. (optional, default = {})
- *Méthode d'action **make** :*
  Effectue une action concrète, éventuellemt à l'aide de la valeur courante. Renvoie le succès ou
  l'échec de sa procédure.
- *Méchanisme d'exécution :*
  La tâche s'effectue selon une certaine probabilité. Le temps d'attente est choisie aléatoirement
  entre les deux bornes fournie. Si **make** réussi, les tâches contenue dans **tasks** seront
  exécuter.

#### Like

Effectue l'action instagram like. La valeur courante doit être Post.

#### Unlike

Effectue l'action instagram unlike. La valeur courante doit être Post.

#### Follow

Effectue l'action instagram follow. La valeur courante doit être User.

#### Unfollow

Effectue l'action instagram unfollow. La valeur courante doit être User.

#### Block

Effectue l'action instagram block. La valeur courante doit être User.

#### Unblock

Effectue l'action instagram unblock. La valeur courante doit être User.

#### GetOwnerFromPost

Récupère l'utilisateur propriétaire de la valeur courante. La valeur courante doit être un Post.

#### GetOwnerFromComment

Récupère l'utilisateur propriétaire de la valeur courante. La valeur courante doit être un Comment.

#### Limiter

Limiter le nombre d'exécutions des sous tâches.

Paramètres :

- **amount** : Nombre d'exécution maximum. (obligatory)

#### ResetLimiter

Reinitialiser le compteur d'un Limiter.

Paramètres :

- **limiter** : La référence vers le Limiter. (obligatory)

#### SaveAsList

La valeur courante doit être User.

Paramètres :

- **username** : le nom de l'utilisateur source. (obligatory)
- **category** : la categorie de la source. (obligatory)

#### SaveOneByOneUser

La valeur courante doit être User.

Paramètres :

- **name** : le nom de la liste. (obligatory)

#### SaveOneByOnePost

La valeur courante doit être Post.

Paramètres :

- **name** : le nom de la liste. (obligatory)

### BrowseTsk

- *Paramètres :*
  - **value** : Selon la tâche, chaîne utilisé comme point de recherche. (possibly obligatory,
    default = "")
  - **path** : Méthode de parcours de la liste de résultats ; valeurs possibles : *FIFO*, *LIFO*, *
    RANDOM*. (optional, default = FIFO)
  - **amount** : Quantité de résultat maximum attendue ; ne fonctionne pas partout. (possibly
    obligatory, default = -1)
  - **tasks** : Listes de tâches. (optional, default = {})
- *Méthode d'action **fillList** :*
  Méthode de remplissage de la liste de résultat. En cas d'échec la liste sera simplement vide et de
  ce fait, aucune sous tâches ne sera exécuter.
- *Méchanisme d'exécution :*
  La tâche commence par demander le remplissage de la liste de résultats à **fillList**. En fonction
  de la valeur de **path**, la liste est réorganisé. Pour chaques objets dans celle-ci, chaques
  tâches est exécuter avec l'objet comme valeur courante.

#### BrowseComment

Recherche les commentaire à partir d'un Post. La valeur courante doit être Post ou **value** doit
être paramétré avec un shortcode.

#### BrowseFollowingOfUser

Recherche les following à partir d'un User. La valeur courante doit être User ou **value** doit être
paramétré avec un username.

#### BrowseFollowersOfUser

Recherche les followers à partir d'un User. La valeur courante doit être User ou **value** doit être
paramétré avec un username.

#### BrowseListFromFile

Charge une liste d'utilisateur enregistré dans un fichier de type *by_list*. Le paramètre **value**
et la valeur courante ne sont pas utilisés.

Paramètres :

- **username** : (obligatory)
- **category** : (obligatory)
- **day** : nombre de jour avant avant celui d'aujourd'hui. (optional, default = 0)

#### BrowseOneByOneUser

Charge une liste de User depuis un fichier *one_by_one* compris dans une intervalle de temps (par
défaut tous les éléments sont chargés). Le paramètre **value**  et la valeur courante ne sont pas
utilisés.

Paramètres :

- **name** : nom de la liste. (obligatory)
- **before** : temps en minutes avant maintenant pour lequel l'entrée doit être antérieur. (
  optional, default = -1)
- **after** : temps en minutes avant maintenant pour lequel l'entrée doit être postérieur. (
  optional, default = -1)

#### BrowseOneByOnePost

Charge une liste de Post depuis un fichier *one_by_one* compris dans une intervalle de temps (par
défaut tous les éléments sont chargés). Le paramètre **value**  et la valeur courante ne sont pas
utilisés.

Paramètres :

- **name** : nom de la liste. (obligatory)
- **before** : temps en minutes avant maintenant pour lequel l'entrée doit être antérieur. (
  optional, default = -1)
- **after** : temps en minutes avant maintenant pour lequel l'entrée doit être postérieur. (
  optional, default = -1)

#### BrowsePostFromTag

Recherche les Post à partir d'un tag instagram selon l'ordre d'affichage normal. Le paramètre **
value** doit être paramétré avec le nom du tag ; la valeur courante n'est pas utilisée.

#### BrowsePostFromUser

Recherche les Post à partir d'un User selon l'ordre d'affichage normal. La valeur courante doit être
User ou **value** doit être paramétré avec un username.

### FilterTsk

- *Paramètres :*
  - **tasksYes** : Listes de tâches exécuter en cas du passage positif de la valeur courante. (
    optional, default = {})
  - **tasksNo** : Listes de tâches exécuter en cas du passage négatif de la valeur courante. (
    optional, default = {})
- *Méthode d'action **isMatch** :*
  Les différents points de tests du filtres ; renvoie le succès l'échec.
- *Méchanisme d'exécution :*
  Selon le résultat de la méthode d'action, l'ensemble des sous tâches correspondante sont exécuter.

#### InstaObjFilter

- *Paramètres :*
  - **intValues** :
  - **boolValues** :
  - **stringValues** :
- *Méchanisme d'exécution :*

##### PostFilter

Paramètres :

- **before**
- **after**
- **intValues**
  - **minimum like amount**
  - **maximum like amount**
  - **minimum comment amount**
  - **maximum comment amount**
- **boolValues**
  - **is video**
  - **comment disabled**
  - **comment disabled for viewer**
  - **is paid partnership**
  - **is affiliate**
  - **viewer has liked**
  - **viewer has saved**
  - **viewer has saved to collection**
  - **is ad**
- **stringValues**
  - **description exclude pattern**
  - **description include pattern**

##### PostFilter

Paramètres :

- **minRatio**
- **maxRatio**
- **intValues**
  - **minumum followers amount**
  - **maximum followers amount**
  - **minumum following amount**
  - **maximum following amount**
  - **minumum post amount**
  - **maximum post amount**
- **boolValues**
  - **is private**
  - **is business**
  - **is verified**
  - **avatar is setup**
- **stringValues**
  - **description exclude pattern**
  - **description include pattern**

#### ListFilter

- *Paramètres :*
  - **inputTasks** : Listes de tâches exécuter pour remplir une liste de comparaison. Pour que le
    filtre puisse récupérer les résultats, il devra nécessairement se retrouvé en sous tâches de
    l'une d'elles. (optional, default = {})
- *Méchanisme d'exécution :*
  Lors de sa première exécution les tâches **inputTasks** sont exécuter. Ensuite c'est la séquence
  normal du filtrage qui s'exécute.

##### UserInList

La valeur courante doit être User, sa présence est vérifier dans la liste du filtre.

##### PostInList

La valeur courante doit être Post, sa présence est vérifier dans la liste du filtre.

## Exemples

### like_by_tag

Cette configuration envisage une situation où un utilisateur cherche du contenue depuis un tag
donné. Ce profile permet de récupérer 50 posts depuis le tag pursuitofportraits. Pour chaque post on
vérifie que son propriétaire n'a pas déjà était la cible du robot (sous réserve que la liste noire
soit correctement utilisé) la semaine précédente, puis qu'il correspond à certain critères. Si tel
est le cas, 3 posts sont prélever au hasard de ses publications puis eventuellement liké et
commenté : dans ces cas, une nouvelles entrées est ajoutéà la liste noire.

Le fichier de configuration générale *global_settings.yml* :

```yaml
!Settings

# likeMaxAmount: 100
# followMaxAmount: 75
# commentsMaxAmount: 25

profilePlans:
  - !profilePlan
    name: like_by_tag  # Le fichier du profile est like_by_tag.yml
    delay: 0           # Le profile sera exécuter immédiatement.
    repeat: 1          # Le profile sera exécuter une fois.

comments:
  my_default:
    - Hello world from ASAFI
    - And another one...
  cmt_multiple_likes:
    - Super!! 😀
    - Marvelous stuff 👏
    - I like your stuff 👌
    - Nice content!
    - Your work is awesome! 😍
    - Great images! 😍

```

Le fichier du profile *like_by_tag.yml* :

```yaml
!Profile

username:
password:

tasks:
  - !BrowsePostFromTag
    value: pursuitofportraits
    amount: 50
    tasks:
      - !GetOwnerFromPost
        minWaitTime: 15
        maxWaitTime: 30
        tasks:
          - &uil !UserInList
            inputTasks:
              - !BrowseOneByOneUser
                name: dynamic_blacklist
                before: 10080
                tasks:
                  - *uil
            tasksNo:
              - !UserFilter
                intValues:
                  minimum following amount: 50
                  maximum following amount: 2500
                  minimum followers amount: 180
                  maximum followers amount: 1200
                  minimum post amount: 9
                  maximum post amount: 500
                boolValues:
                  avatar is setup: true
                minRatio: 0.5
                maxRatio: 2.2
                tasksYes:
                  - !ResetLimiter
                    limiter: &rl !Limiter
                      amount: 3
                      tasks:
                        - !Like
                          probability: 0.3
                          minWaitTime: 2
                          maxWaitTime: 10
                          tasks:
                            - &gofp !GetOwnerFromPost
                              tasks:
                                - !SaveOneByOneUser
                                  name: dynamic_blacklist

                        - !Comment
                          source: cmt_multiple_likes
                          probability: 0.3
                          tasks:
                            - *gofp
                  - !BrowsePostFromUser
                    amount: 15
                    path: random
                    tasks:
                      - !PostFilter
                        intValues:
                          maximum like amount: 600
                          maximum comment amount: 50
                        stringValues:
                          description exclude pattern: (nsfw|#dog|#cat)
                        tasksYes:
                          - *rl

```

![like_by_tag-flowchart](./res/like_by_tag.png)
